# About standards

v.2.0 means 2020th year.

## Requests

* `?` - a new request
* `!` - an approved request
* `+` - a released request

## 14.01.2020

* `+` Add lambda syntax `lambda (args.., catches..) : type { code }`
* `+` Add lambda methods `run` and `runAsync`
* `+` Add prototype syntax `prototype identifier [object]`
* `+` Add prototype inheritance `prototype child extending parent`
* `+` Add methods in objects `[m = method (args..) : type { code }]`
* `+` This pointer in methods is `@`
* `+` Rename `lambda-icl` to `code`
* `+` Rename `lambda-js` to `javascript`
* `+` Rename `js-file` to `script`
* `+` Rename `lambda-sql` to `sql`
* `+` Add generics support: `any`
* `+` Remove any `.query*` method
* `+` Remove search methods `button`, `h1-h6`, `legend`, `span`
* `+` Rewrite completely search methods
* `+` Switch modifiers from `:[]` syntax to `-` syntax

All approved requests will be applied in icL Pro 2.0.1
