@empty = "";
@fonts = "Arial, Helvetica, Times, Courier";

@empty'empty; `` true
@fonts'empty; `` false

@empty'length; `` 0
@fonts'length; `` 32

@empty'last; `` error
@fonts'last; `` "r"

@empty'0; `` error
@fonts'0; `` "A"
