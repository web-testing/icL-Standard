% !TeX spellcheck = en_US
\section{Strings}

{\bf The strings} (\str{} type) are fragments of text.

\subsection{Properties}

The strings have following properties —
\begin{icItems}
\item
	\mintinline{icl}{[r/o] string'empty : bool};
\item
	\mintinline{icl}{[r/o] string'length : int};
\item
	\mintinline{icl}{[r/o] string'last : string};
\item
	\mintinline{icl}{[r/o] string'(n : int) : string}.
\end{icItems}

\ 

Using of string properties —
\inputminted[linenos]{icl}{../sources/stringprop.icL}

\subsubsection{\mintinline{icl}{[r/o] string'empty : bool}}

A string is considered empty if its length is equal with zero.

\subsubsection{\mintinline{icl}{[r/o] string'length : int}}

The length of a string is the number of characters in it.

\subsubsection{\mintinline{icl}{[r/o] string'last : char}}

The last character of string — \mintinline{icl}{string.at (string'length - 1)}.

Possible exceptions: \ferror{EmptyString} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] string'(<int> n) : string}}

The nth character. n must be an integer literal, examples — \mintinline{icl}{@str'0; @string'2}.

Possible exceptions: \ferror{OutOfBounds} (see table \ref{errors}).

\subsection{Methods}

The strings have following methods —
\begin{icItems}
	\item
	\mintinline{icl}{string.append (str : string) : string};
	\item
	\mintinline{icl}{string.at (i : int) : string};
	\item
	\mintinline{icl}{string.beginsWith (str : string) : bool};
	\item
	\mintinline{icl}{string.compare (str : string, caseSensitive = true) : bool};
	\item
	\mintinline{icl}{string.count (str : string) : int};
	\item
	\mintinline{icl}{string.endsWith (str : string) : bool};
	\item
	\mintinline{icl}{string.indexOf (str : string, startPos = 0) : int};
	\item
	\mintinline{icl}{string.insert (str : string, pos : int) : string};
	\item
	\mintinline{icl}{string.lastIndexOf (str : string, startPos = -1) : int};
	\item
	\mintinline{icl}{string.left (n : int) : string};
	\item
	\mintinline{icl}{string.leftJustified (width : int, fillChar) : string, truncate = false :}\\*\mintinline{icl}{string};
	\item
	\mintinline{icl}{string.mid (pos : int, n = -1) : string};
	\item
	\mintinline{icl}{string.prepend (str : string) : string};
	\item
	\mintinline{icl}{string.remove (pos : int, n : int) : string};
	\item
	\mintinline{icl}{string.remove (str : string, caseSensitive = true) : string};
	\item
	\mintinline{icl}{string.replace (pos : int, n : int, after : string) : string};
	\item
	\mintinline{icl}{string.replace (before : string, after : string) : string};
	\item
	\mintinline{icl}{string.right (n : int) : string};
	\item
	\mintinline{icl}{string.rightJustified (width : int, fillChar : string, truncate = false) :}\\* \mintinline{icl}{string};
	\item
	\mintinline{icl}{string.split (separator : string, keepEmptyParts = true, caseSensitive = true) } \mintinline{icl}{: list};
	\item
	\mintinline{icl}{string.substring (begin : int, end : int) : string};
	\item
	\mintinline{icl}{string.toLowerCase () : string};
	\item
	\mintinline{icl}{string.toUpperCase () : string};
	\item
	\mintinline{icl}{string.trim (justWhitespace = true) : string}.
\end{icItems}

Some methods are omitted and will be enumerated in section \ref{regex}.

\newpage
Using of string methods —
\inputminted[linenos]{icl}{../sources/stringmethods.icL}

\subsubsection{\mintinline{icl}{string.append (str : string) : string}}

Appends the string \mintinline{icl}{str} onto the end of this string.

\subsubsection{\mintinline{icl}{string.at (i : int) : string}}

Returns the character at the given index \mintinline{icl}{i} in the string.

Possible exceptions: \ferror{OutOfBounds} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{string.beginsWith (str : string) : bool}}

Returns \true{} if the string begins with \mintinline{icl}{str}; otherwise returns \false.

\subsubsection{\mintinline{icl}{string.compare (str : string, caseSensitive = true) : bool}}

Performs a comparison of this and \mintinline{icl}{str}, using the case sensitivity setting \\*\mintinline{icl}{caseSensitive}.

\subsubsection{\mintinline{icl}{string.count (str : string) : int}}

Returns the number of (potentially overlapping) occurrences of the string \mintinline{icl}{str} in this string.

\subsubsection{\mintinline{icl}{string.endsWith (str : string) : bool}}

Returns \true{} if the string ends with \mintinline{icl}{str}; otherwise returns \false.

\subsubsection{\mintinline{icl}{string.indexOf (str : string, startPos = 0) : int}}

Returns the index position of the first occurrence of the string \mintinline{icl}{str} in this string, searching forward from index position \mintinline{icl}{startPos}. Returns \mintinline{icl}{-1} if \mintinline{icl}{str} is not found.

\subsubsection{\mintinline{icl}{string.insert (str : string, pos : int) : string}}

Inserts the string \mintinline{icl}{str} at the given index position \mintinline{icl}{pos} and returns a reference to this string.

\subsubsection{\mintinline{icl}{string.lastIndexOf (str : string, startPos = -1) : int}}

Returns the index position of the last occurrence of the string \mintinline{icl}{str} in this string, searching backward from index position \mintinline{icl}{startPos}. If \mintinline{icl}{startPos} is \mintinline{icl}{-1} (default), the search starts at the last character; if \mintinline{icl}{startPos} is \mintinline{icl}{-2}, at the next to last character and so on. Returns \mintinline{icl}{-1} if \mintinline{icl}{str} is not found.

\subsubsection{\mintinline{icl}{string.left (n : int) : string}}

Returns a substring that contains the \mintinline{icl}{n} leftmost characters of the string.

\subsubsection{\mintinline{icl}{string.leftJustified (width : int, fillChar) : string, truncate = false :}\\* \mintinline{icl}{string}}

Returns a string of size \mintinline{icl}{width} that contains this string padded by the \mintinline{icl}{fillChar} character.

If truncate is \false{} and the \mintinline{icl}{'length} of the string is more than \mintinline{icl}{width}, then the returned string is a copy of the string.

\subsubsection{\mintinline{icl}{string.mid (pos : int, n = -1) : string}}

Returns a string that contains \mintinline{icl}{n} characters of this string, starting at the specified position \mintinline{icl}{pos}.

Returns a null string if the position \mintinline{icl}{pos} exceeds the length of the string. If there are less than \mintinline{icl}{n} characters available in the string starting at the given position, or if \mintinline{icl}{n} is \mintinline{icl}{-1} (default), the function returns all characters that are available from the specified position.

\subsubsection{\mintinline{icl}{string.prepend (str : string) : string}}

Prepends the string \mintinline{icl}{str} to the beginning of this string and returns a reference to this string.

\subsubsection{\mintinline{icl}{string.remove (pos : int, n : int) : string}}

Removes \mintinline{icl}{n} characters from the string, starting at the given position \mintinline{icl}{pos}, and returns a reference to the string.

If the specified position \mintinline{icl}{pos} is within the string, but \mintinline{icl}{pos + n} is beyond the end of the string, the string is truncated at the specified position.

\subsubsection{\mintinline{icl}{string.remove (str : string, caseSensitive = true) : string}}

Removes every occurrence of the given \mintinline{icl}{str} string in this string, and returns a reference to this string.

If \mintinline{icl}{caseSensitive} is \true{} (default), the search is case sensitive; otherwise the search is case insensitive.

\subsubsection{\mintinline{icl}{string.replace (pos : int, n : int, after : string) : string}}

Replaces \mintinline{icl}{n} characters beginning at index position \mintinline{icl}{pos} with the string \mintinline{icl}{after} and returns a reference to this string.

Note: If the specified position \mintinline{icl}{pos} is within the string, but \mintinline{icl}{pos + n} goes outside the strings range, then \mintinline{icl}{n} will be adjusted to stop at the end of the string.

\subsubsection{\mintinline{icl}{string.replace (before : string, after : string) : string}}

Replaces every occurrence of the string \mintinline{icl}{before} with the string \mintinline{icl}{after} and returns a reference to this string.

\subsubsection{\mintinline{icl}{string.right (n : int) : string}}

Returns a substring that contains the \mintinline{icl}{n} rightmost characters of the string.

The entire string is returned if \mintinline{icl}{n} is greater than or equal to \mintinline{icl}{'length}, or less than zero.

\subsubsection{\mintinline{icl}{string.rightJustified (width : int, fillChar) : string, truncate = false :}\\* \mintinline{icl}{string};}

Returns a string of size \mintinline{icl}{width} that contains the fill character followed by the string.

If truncate is \false{} and the \mintinline{icl}{'lenght} of the string is more than \mintinline{icl}{width}, then the returned string is a copy of the string.

If truncate is \true{} and the \mintinline{icl}{'lenght} of the string is more than \mintinline{icl}{width}, then the resulting string is truncated at position \mintinline{icl}{width}.

\subsubsection{\mintinline{icl}{string.split (separator : string, keepEmptyParts = true, caseSensiti : bool}\\* \mintinline{icl}{, ve = true) : list}}

Splits the string into substrings wherever \mintinline{icl}{separator} occurs, and returns the list of those strings. If \mintinline{icl}{separator} does not match anywhere in the string, returns a single-element list containing this string.

\mintinline{icl}{caseSensitive} specifies whether \mintinline{icl}{separator} should be matched case sensitively or case insensitively.

If \mintinline{icl}{keepEmptyParts} is \false, empty entries don't appear in the result. By default, empty entries are kept.

\subsubsection{\mintinline{icl}{string.substring (begin : int, end : int) : string}}

Returns a string that contains \mintinline{icl}{end - begin} characters of this string, starting at the specified position \mintinline{icl}{begin}.

\subsubsection{\mintinline{icl}{string.toLowerCase () : string}}

Returns a new string, contains the same letters in lower case.

\subsubsection{\mintinline{icl}{string.toUpperCase () : string}}

Returns a new string, contains the same letters in upper case.

\subsubsection{\mintinline{icl}{string.trim (justWhitespace = true) : string}}

Returns a string that has whitespace removed from the start and the end.

If \mintinline{icl}{justWhitespace} is \false{} will be removed any symbols excluding letters and digits (from the start and the end).
